IDP for IBM Domino. It can generate LTPA tokens.

Install
=======

You just have to install the package in your virtualenv and relaunch, it will
be automatically loaded by the plugin framework.

Settings
========

A2_LTPA_TOKEN_SECRET

    Secret to sign tokens, can be plain string, base64 encode with the 'b64:'
    prefix or hex encoded with the 'hex:' prefix. It's mandatory.

A2_LTPA_TOKEN_DURATION

    Lifetime of a token as seconds, default is 3600 (1 hour).

A2_LTPA_COOKIE_NAME

    Name of the cookie to set.

A2_LTPA_COOKIE_DOMAIN

    Domain to set the cookie for cross-domain usage.

A2_LTPA_COOKIE_HTTP_ONLY

    Should the cookie be only sent with HTTP request, default is true.

A2_LTPA_ADAPTER

    Class to adapt username for the LTPA idp, default is
    'authentic2_idp_ltpa.adapter.UserAdapter'

A2_LTPA_TOKEN_USERNAME_ATTRIBUTE:

    Use an attribute from the user to fill the user field of the LTPA token.
