import urlparse

from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.conf import settings
from django.views.decorators.cache import cache_control

from django.contrib.auth.decorators import login_required
from django.contrib.auth import REDIRECT_FIELD_NAME

from . import app_settings, utils, adapter as _adapter

def add_ltpa_token_to_response(request, response):
    adapter = _adapter.get_adapter()
    if not adapter.can_add_token(request):
        return
    if app_settings.TOKEN_SECRET is None:
        raise ImproperlyConfigured('missing TOKEN_SECRET')
    secret = utils.decode_secret(app_settings.TOKEN_SECRET)
    user = adapter.get_username(request)
    token = utils.generate_domino_ltpa_token(user, secret,
            duration=app_settings.TOKEN_DURATION)
    domain = app_settings.COOKIE_DOMAIN or \
    request.META['HTTP_HOST'].split(':')[0]
    max_age = app_settings.COOKIE_DURATION or None
    response.set_cookie(app_settings.COOKIE_NAME, token, domain=domain,
            httponly=app_settings.COOKIE_HTTP_ONLY,
            max_age=max_age)
    request.session['ltpa'] = True

@login_required
@cache_control(no_cache=True, not_store=True, must_revalidate=True)
def ltpa(request):
    '''Ask for authentication then generate a cookie'''
    next_url = request.REQUEST[REDIRECT_FIELD_NAME]
    response = HttpResponseRedirect(next_url)
    add_ltpa_token_to_response(request, response)
    return response

@cache_control(no_cache=True, not_store=True, must_revalidate=True)
def logout(request):
    next_url = urlparse.urljoin(settings.STATIC_URL, 'authentic2/images/ok.png')
    response = HttpResponseRedirect(next_url)
    domain = app_settings.COOKIE_DOMAIN or request.META['HTTP_HOST'].split(':')[0]
    response.delete_cookie(app_settings.COOKIE_NAME, domain=domain)
    return response
