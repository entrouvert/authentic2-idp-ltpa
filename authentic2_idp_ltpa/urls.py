from django.conf.urls import patterns, url

urlpatterns = patterns('authentic2_idp_ltpa.views',
        url('^idp/ltpa/$', 'ltpa', name='ltpa-login'),
        url('^idp/ltpa/logout/$', 'logout', name='ltpa-logout'),
)
