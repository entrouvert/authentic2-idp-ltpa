class AppSettings(object):
    __DEFAULTS = {
            'USE_MIDDLEWARE': True,
            'TOKEN_DURATION': 8*3600,
            'COOKIE_DURATION': 8*3600,
            'TOKEN_SECRET': None,
            'TOKEN_USERNAME_ATTRIBUTE': None,
            'COOKIE_NAME': 'LtpaToken',
            'COOKIE_DOMAIN': None,
            'COOKIE_HTTP_ONLY': True,
            'ADAPTER': 'authentic2_idp_ltpa.adapter.AttributeAdapter',
    }

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt):
        from django.conf import settings
        return getattr(settings, self.prefix + name, dflt)

    @property
    def COOKIE_DURATION(self):
        from django.conf import settings
        default = getattr(settings, 'SESSION_COOKIE_AGE', None)
        return self._setting('COOKIE_DURATION', default)

    def __getattr__(self, name):
        if name not in self.__DEFAULTS:
            raise AttributeError(name)
        return self._setting(name, self.__DEFAULTS[name])


# Ugly? Guido recommends this himself ...
# http://mail.python.org/pipermail/python-ideas/2012-May/014969.html
import sys
app_settings = AppSettings('A2_LTPA_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
