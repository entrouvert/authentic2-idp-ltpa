from django.utils.cache import patch_cache_control

from . import views

class LTPAMiddleware(object):
    def process_response(self, request, response):
        if request.path == '/' or request.path == '/login/':
            views.add_ltpa_token_to_response(request, response)
            # prevent client side caching
            patch_cache_control(response, no_cache=True, no_store=True,
                    must_revalidate=True)
        return response

