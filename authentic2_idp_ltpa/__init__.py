import logging

__version__ = '1.0'

logger = logging.getLogger(__name__)

class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns

    def get_apps(self):
        return [__name__]

    def get_after_middleware(self):
         return ['authentic2_idp_ltpa.middleware.LTPAMiddleware']

    def logout_list(self, request):
        from django.template.loader import render_to_string
        from django.core.urlresolvers import reverse

        from . import app_settings
        if app_settings.COOKIE_NAME not in request.COOKIES:
            return []
        url = reverse('ltpa-logout')
        ctx = {
            'needs_iframe': False,
            'name': 'Domino',
            'url': url,
            'iframe_timeout': 0,
        }
        content = render_to_string('idp/saml/logout_fragment.html', ctx)
        logging.debug('logout %r', content)
        return [content]


from django.utils.six.moves import http_cookies

_new_legal_chars = http_cookies._quote.func_defaults[0] + '/='
http_cookies._quote.func_defaults = (_new_legal_chars,) + http_cookies._quote.func_defaults[1:]
