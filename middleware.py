from . import views

class LTPAMiddleware(object):
    def process_response(self, request, response):
        views.add_ltpa_token_to_response(request, response)
        return response

